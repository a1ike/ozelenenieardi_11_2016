$(document).ready(function() {

  $('#home-video-1').YTPlayer({
    fitToBackground: true,
    videoId: '9CvuKZ1CUPI',
    playerVars: {
      modestbranding: 0,
      autoplay: 1,
      controls: 0,
      showinfo: 0,
      branding: 0,
      rel: 0,
      autohide: 1,
      start: 0
    }
  });

  $('#home-video-2').YTPlayer({
    fitToBackground: true,
    videoId: 'lQMwyW3P3Hw',
    playerVars: {
      modestbranding: 0,
      autoplay: 1,
      controls: 0,
      showinfo: 0,
      branding: 0,
      rel: 0,
      autohide: 1,
      start: 0
    }
  });

  $('#home-video-3').YTPlayer({
    fitToBackground: true,
    videoId: '3F7ZBEg5C54',
    playerVars: {
      modestbranding: 0,
      autoplay: 1,
      controls: 0,
      showinfo: 0,
      branding: 0,
      rel: 0,
      autohide: 1,
      start: 0,
      ratio: 3 / 3
    }
  });

  (function() {
    var Menu = (function() {
      var burger = document.querySelector('.burger');
      var menu = document.querySelector('.menu');
      var menuList = document.querySelector('.menu__list');
      var brand = document.querySelector('.menu__brand');
      var menuItems = document.querySelectorAll('.menu__item');
      var active = false;
      var toggleMenu = function() {
        if (!active) {
          menu.classList.add('menu--active');
          menuList.classList.add('menu__list--active');
          brand.classList.add('menu__brand--active');
          burger.classList.add('burger--close');
          for (var i = 0, ii = menuItems.length; i < ii; i++) {
            menuItems[i].classList.add('menu__item--active');
          }
          active = true;
        } else {
          menu.classList.remove('menu--active');
          menuList.classList.remove('menu__list--active');
          brand.classList.remove('menu__brand--active');
          burger.classList.remove('burger--close');
          for (var i = 0, ii = menuItems.length; i < ii; i++) {
            menuItems[i].classList.remove('menu__item--active');
          }
          active = false;
        }
      };
      var bindActions = function() {
        burger.addEventListener('click', toggleMenu, false);
      };
      var init = function() {
        bindActions();
      };
      return {
        init: init
      };
    }());
    Menu.init();
  }());


  $('#for_dot_1').hover(function() {
    $('#dot_1').addClass('home-dot_active')
  }, function() {
    $('#dot_1').removeClass('home-dot_active');
  });

  $('#for_dot_2').hover(function() {
    $('#dot_2').addClass('home-dot_active')
  }, function() {
    $('#dot_2').removeClass('home-dot_active');
  });

  $('#for_dot_3').hover(function() {
    $('#dot_3').addClass('home-dot_active')
  }, function() {
    $('#dot_3').removeClass('home-dot_active');
  });

  $('#for_dot_4').hover(function() {
    $('#dot_4').addClass('home-dot_active')
  }, function() {
    $('#dot_4').removeClass('home-dot_active');
  });

  $('#for_dot_5').hover(function() {
    $('#dot_5').addClass('home-dot_active')
  }, function() {
    $('#dot_5').removeClass('home-dot_active');
  });

  $('#for_dot_6').hover(function() {
    $('#dot_6').addClass('home-dot_active')
  }, function() {
    $('#dot_6').removeClass('home-dot_active');
  });

  $('#for_dot_7').hover(function() {
    $('#dot_7').addClass('home-dot_active')
  }, function() {
    $('#dot_7').removeClass('home-dot_active');
  });



  $('#dot_1').hover(function() {
    $('#for_dot_1').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_1').removeClass('home-home-nav_active');
  });

  $('#dot_2').hover(function() {
    $('#for_dot_2').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_2').removeClass('home-home-nav_active');
  });

  $('#dot_3').hover(function() {
    $('#for_dot_3').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_3').removeClass('home-home-nav_active');
  });

  $('#dot_4').hover(function() {
    $('#for_dot_4').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_4').removeClass('home-home-nav_active');
  });

  $('#dot_5').hover(function() {
    $('#for_dot_5').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_5').removeClass('home-home-nav_active');
  });

  $('#dot_6').hover(function() {
    $('#for_dot_6').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_6').removeClass('home-home-nav_active');
  });

  $('#dot_7').hover(function() {
    $('#for_dot_7').addClass('home-home-nav_active')
  }, function() {
    $('#for_dot_7').removeClass('home-home-nav_active');
  });

});